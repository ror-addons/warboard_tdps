<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="WarBoard_TDPS" version="1.1.1" date="11/16/2008" >
        
        <Author name="Tortall" email="Email" />
        <Description text="A WarBoard mod for Tortall's DPS Meter." />
        
        <Dependencies>
            <Dependency name="WarBoard" />
            <Dependency name="Tortall_DPS" />
        </Dependencies>
        
        <Files>
            <File name="WarBoard_TDPS.lua" />
            <File name="WarBoard_TDPS.xml" />
        </Files>
        
        <OnInitialize>
            <CallFunction name="WarBoard_TDPS.Initialize" />
        </OnInitialize>
    </UiMod>
</ModuleFile>
