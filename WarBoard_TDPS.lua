WarBoard_TDPS = {}

function WarBoard_TDPS.Initialize()
    if ( WarBoard.AddMod("WarBoard_TDPS") == false ) then return end

    LabelSetText( "WarBoard_TDPSLabel", L"DPS" )

    TortallDPSCore.Register("WarBoard_TDPS", WarBoard_TDPS.Callback)
end

function WarBoard_TDPS.ToggleMeter()
    TortallDPSMeter.Toggle()
end

function WarBoard_TDPS.Callback(damage, healing, time)
    local dps = damage.Dealt.Total.Amount / time
    if ( time == 0.0 ) then dps = 0 end

    LabelSetText( "WarBoard_TDPSDPS", towstring(string.format("%.2f", dps)) )
end

-- The following functions are used in Layout mode. The only argument is the name of the window.
function WarBoard_TDPS.OnDisable()
	WarBoard.DisableMod("WarBoard_TDPS")
end

function WarBoard_TDPS.MoveRight()
	WarBoard.MoveModRight("WarBoard_TDPS")
end

function WarBoard_TDPS.MoveLeft()
	WarBoard.MoveModLeft("WarBoard_TDPS")
end
